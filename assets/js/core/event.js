/**
 * @file Core event delegation & binding.
 * @author <a href="http://davidrekow.com">David Rekow</a>.
 * @copyright 2015
 */

goog.require('$');

goog.provide('event');